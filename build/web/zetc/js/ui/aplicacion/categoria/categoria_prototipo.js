

function Categoria(){
    
   this.tipo = "categoria";   
   this.recurso = "categorias";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Categoria"
   this.tituloplu = "Categorias"   
      
   
   this.campoid=  'categoria';
   this.tablacampos =  ['codigo', 'descripcion' ];
   
   this.etiquetas =  ['Codigo', 'Descripcion' ];
    
   this.tablaformat = ['N',  'C' ];                                  
   
   this.tbody_id = "categoria-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "categoria-acciones";   
         
   this.parent = null;
   
   this.filtro = "";
   
}





Categoria.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
    
    
    var codigo = document.getElementById('categoria_codigo');            
    codigo.focus();
    codigo.select();                
    
};





Categoria.prototype.form_ini = function() {    
  
  
    var codigo = document.getElementById('categoria_codigo');            
     codigo.onblur  = function() {                  
         codigo.value = fmtNum(codigo.value);      
    };      
    codigo.onblur();          
  
    
};






Categoria.prototype.form_validar = function() {    
    
   
    var codigo = document.getElementById('categoria_codigo');        
    if (parseInt(NumQP(codigo.value)) <= 0 )         
    {
        msg.error.mostrar("Falta numero de codigo");    
        codigo.focus();
        codigo.select();                
        return false;
    }        
       
    
    
    var descripcion = document.getElementById('categoria_descripcion');    
    if (descripcion.value == "")         
    {
        msg.error.mostrar("Falta descripcion" );           
        descripcion.focus();
        descripcion.select();        
        return false;
    }       
        

    
    return true;
};










Categoria.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};








Categoria.prototype.icobusqueda = function( obj  ) {      
    
    html.topbar.icobusqueda(obj);    

}





Categoria.prototype.getUrlFiltro = function( obj  ) {                    
    
    var ret = "";    
      
    ret = obj.filtro;
   

    return ret;
    
};


