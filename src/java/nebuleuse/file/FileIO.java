/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package nebuleuse.file;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import nebuleuse.ORM.xml.Global;



public class FileIO { 
    
    
    static final int BUFFER_SIZE = 1024;
    private int bloque = 0;
    

    
    
    public void write(HttpServletRequest request, String pathfile, String append ) 
            throws IOException, ServletException {
        
        
        Part filePart = request.getPart( append );            
        String fileName = filePart.getSubmittedFileName();            
        filePart.write(pathfile);
        
System.out.println("archivo creado : " + append );
        
        
    }
    
    
    

    public void delete ( String pathfile  )  {
        
        try {
                
            File file = new File(pathfile);

            if (file.exists()) {                                                         
                file.delete();   
                System.out.println("archivo eliminado");
            }
        }         
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
        }
    }
    
        
    

    
}
